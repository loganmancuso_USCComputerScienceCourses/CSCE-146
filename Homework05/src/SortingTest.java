import java.util.Arrays;
/**
 * this program will compare the efficiency
 * of selection, bubble, merge, and quick sort
 */
/**
 * @author Logan
 *
 */
public class SortingTest {
	public static int 
			selectionCount=0, bubbleCount=0, mergeCount=0, quickCount=0,
			finSelCnt=0, finBubCnt=0, finMergCnt=0, finQuicCnt=0;
	//selection sort
	public static int[] selectionSort(int array[]){
		int i, j, minIndex, tmp;
		int n = array.length;
		for(i=0; i<n-1; i++){
			minIndex = i;
			for(j=i+1; j<n; j++){
				if(array[j]<array[minIndex]){ 
					minIndex = j; 
				}
				if(minIndex != i){ //swap
					selectionCount++;
					tmp = array[i];
					array[i] = array[minIndex];
					array[minIndex] = tmp;
				}
			}//end j for loop 
		}//end i for loop
		return array;
	}
	//bubble sort
	public static int[] bubbleSort(int array[]){
		boolean swapped = false;
		int n = array.length;
		do{
			swapped = true;
			for(int i=0; i<n-1; i++){
				if(array[i]>array[i+1]){
					int temp = array[i];
					array[i] = array[i+1];
					array[i+1] = temp;
					swapped=false;
				}
				bubbleCount++;
			}
		}while(swapped==false);
		return array;
	}
	//merge sort
	public static void mergeSort(int a[]){
		int size = a.length;
		if(size<2){ //Halting condition
			return;
		}
		int middle = size/2, 
			leftSize = middle, 
			rightSize=(size-middle),
			left[] = new int[leftSize],
			right[] = new int[rightSize];
		//populate left 
		for(int i=0; i<middle; i++){
			left[i] = a[i];
		}
		for(int i=middle; i<size; i++){
			right[i-middle] = a[i]; //if wanting to add middle to i 
		}
		//recursive
		mergeSort(left);
		mergeSort(right);
		//merge back
		merge(left,right,a);
	}
	//supporting method for merge 
	public static void merge(int left[], int right[], int a[]){
		int leftSize = left.length,
			rightSize = right.length,
			i=0,//index left
			j=0, //index right
			k=0; //index a
		while(i<leftSize && j<rightSize){
			if(left[i] <= right[j]){
				a[k] = left[i];
				i++; //move left counter up
				k++; //move final a[] up 
			}else{
				a[k] = right[j];
				j++; //move right counter up
				k++; //move final a[] up
			}
		}
		//fill in rest of a []
		while(i<leftSize){ //if left != empty 
			a[k] = left[i];
			i++;
			k++;
			mergeCount++;
		}
		while(j<rightSize){ //if right != empty 
			a[k] = right[j];
			j++;
			k++;
			mergeCount++;
		}
		mergeCount++;
	}
	//quick sort method 
	public static int[] quickSort(int array[], int left, int right){
		int index = partition(array,left,right); //call pivot method
		if(left<index-1){
			quickSort(array,left,index-1);
		}if(index<right){
			quickSort(array,index+1,right);
		}return array;
	}
	//supporting method for quick 
	public static int partition(int array[], int left, int right){
		int i = left,
			j = right,
			pivot = array[(left+right)/2];
		while(i<=j){
			while(array[i]<pivot){
				quickCount++;
				i++; //correct position move forward
			}while(array[j]>pivot){
				quickCount++;
				j--; //correct move backward
			}if(i<=j){
				int temp = array[i];
				array[i]= array[j];
				array[j] = temp;
				i++; j--;	
			}
		}return i;
	}
	//main method
	public static void main(String[] args){
		int count = 0; //set number of loops
		while (count<20){ //change to 20 once done error checking 
			int selectionSortArray[] = new int[1000];
			int bubbleSortArray[] = new int[1000];
			int mergeSortArray[] = new int[1000];
			int quickSortArray[] = new int[1000];
			for (int i=0; i<1000; i++){
				selectionSortArray[i] = (int)(Math.random()*(999)); //construct random array of 0-999
				bubbleSortArray[i] = (int)(Math.random()*(999));
				mergeSortArray[i] = (int)(Math.random()*(999)); 
				quickSortArray[i] = (int)(Math.random()*(999)); 
			}
			selectionSort(selectionSortArray); 
			bubbleSort(bubbleSortArray);
			mergeSort(mergeSortArray);
			quickSort(quickSortArray,0,quickSortArray.length-1);
			System.out.println("Using Selection: "+selectionCount+
								"\nUsing Bubble: "+bubbleCount+
								"\nUsing Merge: "+mergeCount+
								"\nUsing Quick: "+quickCount+"\n");
			finSelCnt+=selectionCount;
			finBubCnt+=bubbleCount;
			finMergCnt+=mergeCount;
			finQuicCnt+=quickCount;
			selectionCount=bubbleCount=mergeCount=quickCount=0;
			count ++;
		}
		System.out.println("Average:\nSelection: "+finSelCnt/count+"\nBubble: "+finBubCnt/count
				+"\nMerge: "+finMergCnt/count+"\nQuick: "+finQuicCnt/count);
	}//end main
}
