/**
 * 
 */

/**
 * @author Logan
 *
 */
public class TooFewOperandExcep extends Exception {
	//default
	public TooFewOperandExcep(){
		super("Not enough operands in stack to preform an operation");
	}
	//param for err msg
	public TooFewOperandExcep(String aMsg){
		super(aMsg);
	}
}