import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.Stack;

/**
 * 
 */

/**
 * @author Logan
 *
 */
public class BuiltInStructures {
	//main merge method 
	public static void mergeSort(ArrayList<Integer> array){
		if (array.size()<2) { //Halting condition 
			return;
		}else{
			int middle = array.size()/2; //find middle index 
			ArrayList<Integer> leftSide = new ArrayList<Integer>(middle); //construct array list of size middle  
			ArrayList<Integer> rightSide = new ArrayList<Integer>(array.size()-middle); //array list of size other half
			for(int i=0; i<middle; i++){ 
				leftSide.add(array.get(i)); //populate with array index
			}
			for (int i=middle; i<array.size(); i++){
				rightSide.add(array.get(i)); //populate with array index
			}
			//recursive call
			mergeSort(leftSide);
			mergeSort(rightSide);
			//call supporting method for merge sort
			supportMerge(array,leftSide,rightSide);
		}
	}
	//supporting method for merge sort
	public static void supportMerge(
				ArrayList<Integer> array, 
				ArrayList<Integer> left, 
				ArrayList<Integer> right){ 
		int leftI=0, rightI=0, arrayI=0; //index for left,right, and array
		while(leftI<left.size() && rightI<right.size()){
			if(left.get(leftI) <= right.get(rightI)){
				array.set(arrayI, left.get(leftI));
				leftI++;
				arrayI++;
			}else{
				array.set(arrayI, right.get(rightI));
				rightI++;
				arrayI++;
			}
		}
		//left over elements in unmerged arraylists 
		while(leftI<left.size()){
			array.set(arrayI, left.get(leftI));
			leftI++;
			arrayI++;
		}
		while(rightI<right.size()){
			array.set(arrayI, right.get(rightI));
			rightI++;
			arrayI++;
		}
	}
	//main method
	public static void main (String[] args){
		/*
		 * array list of size 10 to 20 
		 * populate with random 0 -99 
		 * print to console
		 * 
		 * sort array list
		 * 
		 * populate queue dequeue and print to console
		 * 
		 * populate stack pop and print to console
		 * 
		 * repeat 3 times 
		 */
		Random r = new Random(); 
		int count = 0;
		do{
			System.out.println("Trial Number: "+(count+1));
			int size = r.nextInt(10)+10;
			System.out.print("\nPopulating ArrayList of Size: "+size+"\n");
			ArrayList<Integer> arrayList = new ArrayList<Integer>();
			for(int i=0; i<size; i++){
				arrayList.add(r.nextInt(99)); //adding to array list
			}
			for(int i : arrayList){
				System.out.print(i+"\t"); //printing
			}
			System.out.println("\nSorted ArrayList:");
			mergeSort(arrayList); //sorting the array using merge sort
			for(int i : arrayList){
				System.out.print(i+"\t"); //printing
			}
			Queue <Integer> queue = new LinkedList<Integer>();
			for(int i=0;i<size;i++){
				queue.add(arrayList.get(i)); //adding to queue, enqueue
			}
			System.out.println("\nDequeue:");
			while(queue.isEmpty()==false){
				System.out.print(queue.remove()+"\t"); //printing, dequeue
			}
			Stack <Integer> stack = new Stack <Integer>();
			for(int i=0;i<size;i++){
				stack.push(arrayList.get(i)); //adding to stack, push
			}
			System.out.println("\nPopping:");
			while(stack.isEmpty()==false){
				System.out.print(stack.pop()+"\t"); //printing, pop
			}
			count ++;
			System.out.println("\n");
		}while(count<3);
	}
}
