/**
 * 
 */

/**
 * @author Logan
 *
 */
public interface Shape <Type extends Comparable>{
	public String getType(); //type of shape, circle, rectangle, right triangle
	public double getArea();  //access to area from aNode.data
	public double getPerimeter(); //same as area 
	public void print(); //custom print method to keep from having to pass up data 
}
