/**
 * 
 */
import java.util.Scanner;
/**
 * @author Logan
 *
 */
public class PhobiaDictionary {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PhobiaDatabase dictionary = new PhobiaDatabase();
		System.out.println("Welcome to the phobia dictionary");
		int choice;
		do{
			System.out.println(""
					+ "1: add\n"
					+ "2: remove\n"
					+ "3: find\n"
					+ "4: print\n"
					+ "9: quit");
			Scanner kybd = new Scanner(System.in);
			choice = kybd.nextInt();
			kybd.nextLine();
			switch(choice){
			case 1:
				System.out.println("Enter a Fear");
				String name = kybd.nextLine().trim();
				System.out.println("Enter the Decsription");
				String descrip = kybd.nextLine().trim();
				Phobia newPhobia = new Phobia(name,descrip);
				dictionary.insert(newPhobia);
				break;
			case 2:
				System.out.println("Enter a Fear to Remove");
				String nameToRemove = kybd.nextLine().trim();
				System.out.println("Enter the Decsription");
				String description = kybd.nextLine().trim();
				Phobia aNewPhobia = new Phobia(nameToRemove,description);
				dictionary.remove(aNewPhobia);
				break;
			case 3:
				System.out.println("Enter a phobia to find");
				String toFind = kybd.nextLine().trim();
				dictionary.find(toFind);
				break;
			case 4:
				System.out.println("Printing the dictionary");
				dictionary.print();
				break;
			case 9:
				break;
			}
			
		}while(choice != 9);
	}

}
