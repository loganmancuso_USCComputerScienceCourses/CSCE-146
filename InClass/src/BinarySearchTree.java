/**
 * extends comparable of type 'Type'
 * left= lower value, right= higher value 
 * based on parent value not root 
 * 
 */

/**
 * @author Logan
 *  
 */
public class BinarySearchTree <Type extends Comparable<Type>> { //ensures comparability 
	//private class of node to create a linked structure 
	private class Node{ //every instance is a node within a tree
		private Type dataInNode; //generic type 'Type' of data
		private Node leftChild, rightChild; //branching left and right 
		public Node(){ //default constructor 
			this.dataInNode = null;
			this.leftChild = null; 
			this.rightChild = null;
		}
	}
	private Node root;
	public BinarySearchTree(){
		this.root = null; 
	}
	//insertion method  
	public void insertInTree(Type data){
		Node nodeToAdd = new Node();
		nodeToAdd.dataInNode = data;
		if(root==null){//empty tree
			root = nodeToAdd; //place at root 
		}else{
			toInsert(root,data);
		}
	}
	// insert helper method move to find null by comparing < or > of value with in the node 
	private Node toInsert(Node aNode, Type data){
		if(aNode == null){ //if found spot and is empty place 
			Node nodeToAdd = new Node();
			nodeToAdd.dataInNode = data;
			aNode = nodeToAdd;
		}else if(data.compareTo(aNode.dataInNode)<0){ //go left, data<currentnNode.dataInNode
			aNode.leftChild=toInsert(aNode.leftChild,data);
		}else if(data.compareTo(aNode.dataInNode)>=0){ //go right, data>=currentnNode.dataInNode
			aNode.rightChild=toInsert(aNode.rightChild,data);
		}
		return aNode; 
	}
	//search 
	public boolean searchInTree(Type data){
		return toSearch(root,data);
	}
	//searching helper method
	private boolean toSearch(Node aNode, Type data){
		if(aNode==null){//not in tree
			return false;
		}else if(aNode.dataInNode.compareTo(data)==0){//found in tree
			return true;
		}else if(aNode.dataInNode.compareTo(data)>0){//go left
			return toSearch(aNode.leftChild,data);
		}else{//go right 
			return toSearch(aNode.rightChild,data);
		}
	}
	//delete passed in value
	public void deleteInTree(Type value){
		root= toDelete(root, value);
	}
	private Node toDelete(Node aNode, Type data){  
		if (aNode == null){ //not in tree
			return null;
		}else if(data.compareTo(aNode.dataInNode)<0){ //go left data<current.data
			aNode.leftChild = toDelete(aNode.leftChild,data);
		}else if(data.compareTo(aNode.dataInNode)>0){ //go right, data>=current.data
			aNode.rightChild = toDelete(aNode.rightChild,data);
		}else if(data.compareTo(aNode.dataInNode)==0){ //found value
			if(aNode.rightChild == null){//if no right child 
				return aNode.leftChild;
			}else if(aNode.leftChild == null){//is a right but not a left 
				return aNode.rightChild;
			}//node has two children 
			Node tmp = aNode; //holds original value to delete 
			aNode = minValue(aNode.rightChild); //find leftmost of right subtree
			aNode.rightChild = deleteMin(tmp.rightChild); //delete minimum 
			aNode.leftChild = tmp.leftChild; //new link 
		}
		return aNode; 
	}
	//find least value in the tree if not reached move left until null 
	private Node minValue(Node aNode){
		if(aNode == null){ //empty tree
			return null; 
		}else if(aNode.leftChild == null){ //found least value 
			return aNode;
		}else{ //child still pointing to a node continue left
			return minValue(aNode.leftChild);
		}
	}
	private Node deleteMin(Node aNode){
		if(aNode == null){
			return null; //no tree
		}else if(aNode.leftChild == null){ //greatest has been found
			return aNode.rightChild;
		}else{ //right child has value greater move right
			aNode.leftChild = deleteMin(aNode.leftChild);
		}
		return aNode; 
	}
	public void printInOrder(){
		System.out.println("Pre Order");
		printPreOrder(root);
		System.out.println("In Order");
		printInOrder(root); 
		System.out.println("Post Order");
		printPostOrder(root);
	}
	private void printInOrder(Node aNode){
		if(aNode == null){
			return;
		}if(aNode.leftChild != null){
			printInOrder(aNode.leftChild);
			//move print here 
		}
		System.out.println(aNode.dataInNode.toString());
		if(aNode.rightChild != null){
			printInOrder(aNode.rightChild);
			//move print here 
		}
		return;
	}
	private void printPreOrder(Node aNode){
		if(aNode == null){
			return;
		}
		System.out.println(aNode.dataInNode.toString());	
		if(aNode.leftChild != null){
			printPreOrder(aNode.leftChild);
		}
		if(aNode.rightChild != null){
			printPreOrder(aNode.rightChild);
			//move print here 
		}
		return;
	}
	private void printPostOrder(Node aNode){
		if(aNode == null){
			return;
		}if(aNode.leftChild != null){
			printPostOrder(aNode.leftChild);
		}
		if(aNode.rightChild != null){
			printPostOrder(aNode.rightChild);
		}
		System.out.println(aNode.dataInNode.toString());
		return;
	}
	/*
	 * main method 
	 */
	public static void main (String args[]){
		
	}
}
