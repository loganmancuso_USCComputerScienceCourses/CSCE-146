/**
 * 
 */

/**
 * @author Logan
 *
 */

public class SearchAlg {
	
	//Iterative search
	public static boolean linSearIter(int[] a, int val){
		for(int i=0;i<a.length; i++){
			if(a[i] == val){
				return true;
			}
		}return false;
	}
	
	//recursive search 
	public static boolean linSearRecur(int[] a, int val, int index){
		if(a[index] == val){
			return true;
		}else{
			index++;
			if (index >= a.length){
				return false;
			}else{
				linSearRecur(a,val,index);
			}
		}return false;
	} 
	
	//recursive binary search
	public static boolean binSearRecur(int[] a, int val, int minIndex, int maxIndex){
		int middle = (maxIndex + minIndex)/2;
		if(minIndex > maxIndex){ //not in list
			return false;
		}
		if(a[middle]== val){//if middle is chosen val
			return true;		
		}else if(val>a[middle]){ //search top half of sorted array
			return binSearRecur(a,val,middle+1,maxIndex);
		}else if(val<a[middle]){ //search bottom half of sorted array
			return binSearRecur(a,val,minIndex,middle-1);
		}else{
			System.out.println("error");
		}
		return false;
	}
	public static void sortArray(int[] a){ //sorting the array using selection 
		int i, j, minIndex, tmp;
		int n = a.length;
		for(i=0; i<n-1; i++){
			minIndex = i;
			for(j=i+1; j<n; j++){
				if(a[j]<a[minIndex]){ 
					minIndex = j; 
				}
				if(minIndex != i){ //swap
					tmp = a[i];
					a[i] = a[minIndex];
					a[minIndex] = tmp;
				}
			}//end j for loop 
		}//end i for loop
	}
	
	/*
	 * main method
	 */
	
	public static void main(String[] args){
		int[] a = {7, 4 , 6 , 2, 3, 7, 8, 3};
		System.out.print("7, 4 , 6 , 2, 3, 7, 8, 3\n");
		int minIndex = 1;
		int maxIndex = a.length;
		sortArray(a);
		for (int i=0; i<a.length; i++){
			System.out.print(a[i]+"\t");
		}
	}
}
