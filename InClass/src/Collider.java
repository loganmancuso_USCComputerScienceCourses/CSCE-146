/**
 * 
 */

/**
 * @author Logan
 *
 */
public class Collider {

	private double x;
	private double y;
	
	public Collider(){
		this.x = 0.0;
		this.y = 0.0; 
	}
	public Collider(double ax, double ay){
		this.setX(ax);
		this.setY(ay);
	}
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
	
}
