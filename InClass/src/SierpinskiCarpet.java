/**
 * 
 */

/**
 * @author Logan
 *	
 */
import java.util.*;
import java.applet.*;
import java.awt.*;

public class SierpinskiCarpet extends Applet{
	private Image disp;
	private Graphics drwArea;
	public void init(){
		int h = super.getSize().height;
		int w = super.getSize().width;
		disp = createImage(w,h);
		drwArea = disp.getGraphics();
		//draw carpet
		drawCarpet(0,0,h,drwArea);
	}
	public void paint(Graphics g){
		g.drawImage(disp, 0, 0, null);
	}
	public static void drawCarpet(int x, int y, int side, Graphics g){
		int sub = side/3;
		g.fillRect(x+sub, y+sub, sub, sub);
		if(sub >= 3){
			//top 3 squares
			drawCarpet(x,y,sub,g); //left
			drawCarpet(x+sub,y,sub,g); //middle
			drawCarpet(x+sub*2,y,sub,g); //right
			//middle two
			drawCarpet(x,y+sub,sub,g); //left
			drawCarpet(x+sub*2,y+sub,sub,g); //right
			//bottom 3 squares
			drawCarpet(x,y+sub*2,sub,g); //left
			drawCarpet(x+sub,y+sub*2,sub,g); //middle
			drawCarpet(x+sub*2,y+sub*2,sub,g); //right
		}
	}
}
