/**
 * 
 */
import java.util.*;

/**
 * @author Logan
 *
 */
public class QuadTree {

	private class Node{
		private ArrayList<Collider> collider;
		private double x,y,sideLenght; //top left positions (x,y), length of side 
		private Node TL,TR,BL,BR;
		public Node(double aX, double aY, double aSL){
			this.x = aX;
			this.y = aY;
			this.sideLenght = aSL;
			this.collider = new ArrayList<Collider>();
		}
	}
	private Node root;
	private int depth; //times to split environment 
	public QuadTree(double aSideLenght, int aDepth){
		root = subDivideNode(0,0,aSideLenght,0);//subdivide
	}
	private Node subDivideNode(double aX, double aY, double aSideLenght, int aDepth){
		
		Node aNode = new Node(aX,aY,aSideLenght);
		if(aDepth > 0){
			double half = aSideLenght / 2.0; 
			aNode.TL = subDivideNode(aX,aY,half,aDepth-1);
			aNode.TR = subDivideNode(aX+half,aY,half,aDepth-1);
			aNode.BL = subDivideNode(aX,aY+half,half,aDepth-1);
			aNode.BR = subDivideNode(aX+half,aY+half,half,aDepth-1);
		}
		return aNode;
	}
	public void addCollider(Collider aCollider){
		addCollider(root, aCollider);
	}
	public void addCollider(Node aNode, Collider aCollider){
		
	}
	private boolean isContained(Node aNode, Collider aCollider){
		return false; //TODO: change  
	}
	private boolean isContained(Node aNode, CircleCollider aCollider){
		if(aNode == null) return false;
		// find center 
		double boxCenterX = aNode.x + aNode.sideLenght/2.0;
		double boxCenterY = aNode.y + aNode.sideLenght/2.0;
		//calculate vector 
		double vectX = aCollider.getX() - boxCenterX;
		double vectY = aCollider.getY() - boxCenterY;
		//magnitude of that vector 
		double magnitude = Math.sqrt(vectX*vectX + vectY*vectY);
		//
		double normalX = vectX/magnitude;
		double normalY = vectY/magnitude;
		//point on the circle 
		double pointOnCircleX = normalX*aCollider.getRadius() + aCollider.getX();
		double pointOnCircleY = normalY*aCollider.getRadius() + aCollider.getY(); 
		
		
		return false;
	}
	
	
}
