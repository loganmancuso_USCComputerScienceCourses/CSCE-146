/**
 * 
 */

/**
 * @author Logan
 *	
 */
import java.awt.*;
import javax.swing.*;

public class SierpinskiTriangle extends JApplet{
	JFrame frame = new JFrame("SierpinskiTriangle"); //name the frame 
	private int redraw = 5; //number of recursions
	public void paint(Graphics g){
		//set new points
		Point x= new Point(getWidth()/2, 5),
				y= new Point(5, getWidth()), 
				z= new Point(getWidth()-5, getWidth()-5);
		drawTriangle(x,y,z,redraw,g);
	}
	/**
	 * @param x
	 * @param y
	 * @param z
	 * @param i
	 * @param g
	 */
	private void drawTriangle(Point x, Point y, Point z, int i, Graphics g) {
		// TODO Auto-generated method stub
		if (i == 0){
			g.drawLine(x.x, x.y, y.x, y.y);
			g.drawLine(x.x, x.y, z.x, z.y);
			g.drawLine(y.x, y.y, z.x, z.y);
		}else{
			Point mx = midpoint(x,y);
			Point my = midpoint(y,z);
			Point mz = midpoint(z,x);
			//recursive call
			drawTriangle(x,mx,mz,i-1,g);
			drawTriangle(mx,y,my,i-1,g);
			drawTriangle(mz,my,z,i-1,g);
		}
	}
	/**
	 * @param x
	 * @param z
	 * @return
	 */
	private Point midpoint(Point a, Point b) {
		// TODO Auto-generated method stub
		return new Point ((a.x+b.x)/2 , (a.y+b.y)/2); //midpoint formula
	}
	
}



