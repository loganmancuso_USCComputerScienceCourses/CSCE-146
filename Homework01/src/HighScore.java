
public class HighScore {

	//intance variables 
	
	private String name;
	private int points;
	
	public HighScore()
	{
		this.name = "no name yet";
		this.points = 0;
	}
	public HighScore(String aName, int aPoint){
		this.setName(aName);
		this.setPoints(aPoint);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}

}
