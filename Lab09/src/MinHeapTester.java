/**
 * 
 */

/**
 * @author Logan
 *
 */
public class MinHeapTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Integer Min Heap Tester\nPopulating with values:");
		ArrayMinHeap heap = new ArrayMinHeap();
		int array[] = {21,37,49,11,23,1,13,16,33,17};
		for (int val : array){
			System.out.println(val);
			heap.insert(val);
		}
		System.out.println("Printing heap:");
		heap.print();
		System.out.println("Testing Heap Sort:");
		heap.heapSort();
		System.out.println("Element to remove is: " + heap.peek());
		heap.delete();
		System.out.println("Printing:");
		heap.print();
	}

}
