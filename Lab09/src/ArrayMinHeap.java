/**
 * 
 */

/**
 * @author Logan
 *
 */
public class ArrayMinHeap {

	public static final int DEF_SIZE = 10; //cdefault size of array 
	private int heap[],size;
	
	public ArrayMinHeap(){
		heap = new int[DEF_SIZE];
		size = 0; 
	}
	public ArrayMinHeap(int size){
		if(size > 0 ){ 
			heap = new int [size]; //change to user desired size 
			size = 0;
		}else{
			return; 
		}
	}

	//insert 
	public void insert(int val){
		if(size >= heap.length){ 
			return;
		}
		heap[size] = val; 
		bubble();
		size++;
	}
	private void bubble(){
		int index = this.size;
		while (index > 0){
			int parent = index%2!=0?(index-1)/2:(index-2)/2;
			if(parent >=0 && heap[index] - heap[parent] < 0){
				//swap 
				int tmp = heap[parent];
				heap[parent] = heap[index];
				heap[index] = tmp;
			}else{
				break;
			}
			index = parent; //move index 
		}
	}
	public int peek(){
		if(heap == null){
			return 0;
		}
		return heap[0];
	}
	//delete 
	public int delete(){
		int retVal = peek();
		if(size > 0){
			heap[0] = heap[size-1];
			heap[size-1] = -999; //for new null value 
			size --;
			bubbleDown();
		}
		return retVal;
	}
	private void bubbleDown(){
		int index = 0;
		while(index*2+1 < size){ // do not change this does not change for min or max heap 
			int smallIndex = index*2+1;
			if(index*2+2 < size && heap[index*2+1] - heap[index*2+2] >0){
				smallIndex = index*2+2;
			}
			if(heap[index] - heap[smallIndex]>0){
				int tmp = heap[index];
				heap[index] = heap[smallIndex];
				heap[smallIndex] = tmp;
			}else{
				break;
			}
			index = smallIndex;
		}
	}
	//print breadth 
	public void print(){
		for(int val : heap){
			if(val != 0 && val != -999) //doesn't print non initialized "0" or null "-999" 
				System.out.println(val);
			else{
				break; 
			}
		}
	}
	//heapsort
	public void heapSort(){
		ArrayMinHeap temp = new ArrayMinHeap(heap.length);
		int deepCopyHeap[] = heap.clone(); //deep copy the heap 
		for(int i=0; i<size; i++){
			temp.insert(deepCopyHeap[i]); //populate 
		}
		for(int i=size; i>=0; i--){
			if(temp.peek() != -999){ //stop from printing null values 
				System.out.print(temp.delete()+"\t");
			}
		}
		System.out.print("\n"); 
	}
}
