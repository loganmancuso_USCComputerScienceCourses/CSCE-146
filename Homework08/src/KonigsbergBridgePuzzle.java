import java.util.*;

/*****************************************************
 * Original Structure 
 * 
 *	            (A) 
 *   _|1|_|2|_       __|4|__
 *  /         \_____/       \
 * |    (B)    __3__   (D)   |
 *  \_   _   _/	    \__   __/
 *    |5| |6|          |7|
 *              (C)
 * 
 *****************************************************/

public class KonigsbergBridgePuzzle {
	private class Vertex {
		String name;
		// Add edges-connect verteces together
		ArrayList<Edge> neighbors;

		public Vertex(String aName) {
			this.name = aName;
			this.neighbors = new ArrayList<Edge>();
		}
	}

	private class Edge {
		Vertex v1;
		double weight;// relationship between vertexs-very important

		public Edge(Vertex aV1, double aWeight) {
			v1 = aV1;
			weight = aWeight;// no need for error checking as no negative weights 
		}
	}

	// origin can be anywhere- we will sets as the first vertex added
	Vertex origin;
	ArrayList<Vertex> verticies;
	ArrayList<Vertex> markedVerticies;

	public KonigsbergBridgePuzzle() {
		origin = null;
		verticies = new ArrayList<Vertex>();
		markedVerticies = new ArrayList<Vertex>();
	}

	public void addVertex(String aName) {
		if (vertexIsContained(aName)) {
			return;// if name already used, does not insert the vertex
		}
		Vertex v = new Vertex(aName);// can have duplicate data- may want to
										// check for this
		verticies.add(v);// assume that is the orgin
		if (origin == null) {
			origin = v;
		}
	}

	public boolean vertexIsContained(String aName) {
		for (Vertex vert : verticies) {
			if (vert.name.equals(aName)) {
				return true;
			}
		}
		return false;
	}

	public void addEdge(String fromVert, String toVert, double weight) {
		Vertex v1 = getVertex(fromVert);
		Vertex v2 = getVertex(toVert);
		if (v1 == null || v2 == null)// error checking
		{
			return;
		}
		v1.neighbors.add(new Edge(v2, weight));
	}

	public Vertex getVertex(String aName) {
		for (Vertex vert : verticies) {
			if (vert.name.equals(aName)) {
				return vert;
			}
		}
		return null;
	}

	public int checkForOddDegree(String aName) {
		Vertex v1 = getVertex(aName);// get a vertex with the name aName
		if (v1.neighbors.size() % 2 != 0)// if it has an odd number of neighbors
		{
			return 1;
		}
		return 0;
	}

}
