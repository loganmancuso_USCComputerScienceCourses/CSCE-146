/**
 * this program will print the Sierpinski triangle fractal
 */

/**
 * @author Logan
 *	
 */
import java.awt.*;
import javax.swing.*;

public class SierpinskiTriangle extends JApplet{
	public static int redraw = 5; //number of times the triangle loops
	public void paint(Graphics g){ //graphics constructor 
		//set new points
		Point x= new Point(getWidth()/2, 5),
				y= new Point(5, getWidth()), 
				z= new Point(getWidth()-5, getWidth()-5);
		/*
		 * construct two integer arrays from points 
		 * one of x points the other of y points
		 * a=x points, b=y points
		 */
		int[] a= {x.x, y.x, z.x},
			b= {x.y, y.y, z.y};
		Polygon p = new Polygon(a,b,3); //create polygon of points from a and b 
		g.setColor(Color.WHITE); //color
		g.fillPolygon(p); 
		drawTriangle(a,b,3,redraw,g); //call method to loop through pass in the number redraw
	}
	/**
	 * @param x
	 * @param y
	 * @param z
	 * @param i
	 * @param g
	 */
	private void drawTriangle(int[] a,int[] b,int numb_points,int i,Graphics g) {
		// TODO Auto-generated method stub
		
		//construct new points from integer array
		Point x= new Point(a[0], b[0]),
				y= new Point(a[1], b[1]), 
				z= new Point(a[2], b[2]);
		if (i == 0){
			int[] a2 = {x.x,y.x,z.x};
			int[] b2 = {x.y,y.y,z.y};
			Polygon p = new Polygon(a2,b2,3);
			g.setColor(Color.BLACK);
			g.fillPolygon(p);
		}else{
			/*
			 * midpoint method (x2-x1)/2 and (y2-y1)/2
			 */
			Point mx = midpoint(x,y); 	
			Point my = midpoint(y,z);
			Point mz = midpoint(z,x);
			/*
			 *new integer arrays of new mid points
			 *then create polygon from points and fill polygon 
			 */
			int[] a3 = {x.x,mx.x,mz.x};
			int[] b3 = {x.y,mx.y,mz.y};
			Polygon p1 = new Polygon(a3,b3,3);
			g.setColor(Color.WHITE);
			g.fillPolygon(p1);
			int[] a4 = {mx.x,y.x,my.x};
			int[] b4 = {mx.y,y.y,my.y};
			Polygon p2 = new Polygon(a4,b4,3);
			g.setColor(Color.WHITE);
			g.fillPolygon(p2);
			int[] a5 = {mz.x,my.x,z.x};
			int[] b5 = {mz.y,my.y,z.y};
			Polygon p3 = new Polygon(a5,b5,3);
			g.setColor(Color.WHITE);
			g.fillPolygon(p3);
			
			//recursive call passing in new integer arrays 
			drawTriangle(a3,b3,3,i-1,g);
			drawTriangle(a4,b4,3,i-1,g);
			drawTriangle(a5,b5,3,i-1,g);
		}
	}
	/**
	 * @param x
	 * @param z
	 * @return
	 */
	private Point midpoint(Point a, Point b) {
		// TODO Auto-generated method stub
		return new Point ((a.x+b.x)/2 , (a.y+b.y)/2); //midpoint formula
	}
}